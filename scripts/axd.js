/*Copyright(C) 2010-2013 Isaiah Gilliland
*
* This file is part of Axis.
*
*    Axis is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Lesser General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Axis is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Lesser General Public License for more details.
*
*    You should have received a copy of the GNU Lesser General Public License
*    along with Axis.  If not, see <http://www.gnu.org/licenses/>.
*/

// axis drawing api, needs to be remade and cleaned up pretty bad

var axd = {};
//draws a circle with an optional border
//and optional color. Use center=true if used to
//draw an actor
axd.cir = function (center, x, y, rad, fill, border){
    if(!center){
        x+=ax.scrollx;
		y+=ax.scrolly;
    }
    var c = ax.ctx;
    c.save();
    c.beginPath();
    c.arc(x,y,rad,0,Math.PI*2,false);
    c.closePath();
    if(fill){
		//add something for transparency
		c.fillStyle = "rgb("+fill[0]+","+fill[1]+","+fill[2]+")";
		c.fill();
    }
    if(border){
		c.lineWidth = border[0];
		c.strokeStyle = "rgb("+border[1]+","+border[2]+","+border[3]+")";
		c.stroke();
    }
    c.restore();
    
};

axd.drawNormals = function(obj){
	var c = ax.ctx;
	//speed
	var n = (obj.speed > 0)?axu.getNormal(obj.vel): [Math.cos(obj.angle/180*Math.PI), Math.sin(obj.angle/180*Math.PI)];
	var rn = [n[1]*-1,n[0]];
	var ln = [n[1],n[0]*-1];
	var length = Math.sqrt(obj.width*obj.width+obj.height*obj.height)/2;
	length = length*(obj.speed/20);
	length = (length < 10 )? 10 : length;
	//draw regular normal
	c.strokeStyle = "rgb(0,250,0)";
	c.beginPath();
	c.moveTo(0,0);
	c.lineTo(n[0]*length,n[1]*length);
	c.closePath();
	c.stroke();
	//draw the direction triangle
	c.beginPath();
	c.moveTo(n[0]*(length+5), n[1]*(length+5));
	c.lineTo(n[0]*(length)+rn[0]*5, n[1]*(length)+rn[1]*5);
	c.lineTo(n[0]*(length)+ln[0]*5, n[1]*(length)+ln[1]*5);
	c.closePath();
	c.stroke();
	//draw left normal
	c.strokeStyle = "rgb(250,250,0)";
	c.beginPath();
	c.moveTo(0,0);
	c.lineTo(ln[0]*length,ln[1]*length);
	c.closePath();
	c.stroke();
	//draw right normal
	c.strokeStyle = "rgb(250,0,0)";
	c.beginPath();
	c.moveTo(0,0);
	c.lineTo(rn[0]*length,rn[1]*length);
	c.closePath();
	c.stroke();
	axd.cir(true,0,0,5,[0,250,0]);
};

axd.poly = function(poly, x, y){
    var ofx = ax.scrollx+x;
    var ofy = ax.scrolly+y;
    var c = ax.ctx;
    
    var i = 1;
    var l = poly.length;
    c.save();
    c.strokeStyle = "rgb(250,0,0)";
    c.beginPath();
    c.moveTo(poly[0][0]+ofx,poly[0][1]+ofy);
    
    while(i < l){
        c.lineTo(poly[i][0]+ofx,poly[i][1]+ofy);
        i++;
    }
    
    c.lineTo(poly[0][0]+ofx,poly[0][1]+ofy);
    c.closePath();
    c.stroke();
    c.restore();
};

axd.rect = function (center,x,y,width,height,fill,border){
    if(!center){
		x+=ax.scrollx;
		y+=ax.scrolly;
    }
    var c = ax.ctx;
    c.save();
    if(fill){
		//add something for transparency
		c.fillStyle = "rgb("+fill[0]+","+fill[1]+","+fill[2]+")";
		c.fillRect(x-width/2,y-height/2,width,height);
    }
    if(border){
		c.lineWidth = border[0];
		c.strokeStyle = "rgb("+border[1]+","+border[2]+","+border[3]+")";
		c.strokeRect(x-width/2,y-height/2,width,height);
    }
    c.restore();
};

axd.image = function (center, image, x, y, width, height){
	if(!image)return;
	width = width || image.width;
	height = height || image.height;
	
    if(!center){
		x+=ax.scrollx;
		y+=ax.scrolly;
    }
    var c = ax.ctx;
    try{
        c.drawImage(image,x-width/2,y-height/2, width, height);
    }catch(e){
        console.log(x-width/2+","+y-height/2);
    }
    //c.drawImage(image,x-image.width/2,y-image.height/2);
};

axd.sprite = function (center,image,cx,cy,cwidth,cheight,dx,dy,dwidth,dheight){
    if(!center){
		dx+=ax.scrollx;
		dy+=ax.scrolly;
    }
    var c = ax.ctx;

    c.save();
    c.drawImage(
        image,
        cx,cy,cwidth,cheight,
        dx-cwidth/2,dy-cheight/2,dwidth,dheight
        );
    c.restore();
};

axd.clr = function (){
	var c = ax.ctx;
    c.setTransform(1,0,0,1,0,0);
    c.clearRect(0,0,ax.width,ax.height);
	//ax.canvas.width = ax.canvas.width;
    
};
