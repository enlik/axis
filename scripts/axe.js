/*Copyright(C) 2010-2013 Isaiah Gilliland
*
* This file is part of Axis.
*
*    Axis is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Lesser General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Axis is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Lesser General Public License for more details.
*
*    You should have received a copy of the GNU Lesser General Public License
*    along with Axis.  If not, see <http://www.gnu.org/licenses/>.
*/

//namespace
var axe = {};

//index of keys and their info
axe.keys = {
    //SPECIAL KEYS
    LEFT:{down:false,up:false,code:37},
    UP:{down:false,up:false,code:38},
    RIGHT:{down:false,up:false,code:39},
    DOWN:{down:false,up:false,code:40},
    SHIFT:{down:false,up:false,code:16},
    CTRL:{down:false,up:false,code:17},
    ALT:{down:false,up:false,code:18},
    SPACE:{down:false,up:false,code:32},
    ENTER:{down:false,up:false,code:13},
    TAB:{down:false,up:false,code:9},
    BSPACE:{down:false,up:false,code:8},
    ESC:{down:false,up:false,code:27},
    //NUMBERS
    "1":{down:false,up:false,code:49},
    "2":{down:false,up:false,code:50},
    "3":{down:false,up:false,code:51},
    "4":{down:false,up:false,code:52},
    "5":{down:false,up:false,code:53},
    "6":{down:false,up:false,code:54},
    "7":{down:false,up:false,code:55},
    "8":{down:false,up:false,code:56},
    "9":{down:false,up:false,code:57},
    "0":{down:false,up:false,code:48},
    //NUM KEYS
    N1:{down:false,up:false,code:97},
    N2:{down:false,up:false,code:98},
    N3:{down:false,up:false,code:99},
    N4:{down:false,up:false,code:100},
    N5:{down:false,up:false,code:101},
    N6:{down:false,up:false,code:102},
    N7:{down:false,up:false,code:103},
    N8:{down:false,up:false,code:104},
    N9:{down:false,up:false,code:105},
    N0:{down:false,up:false,code:96},
    //SHIFT + KEY
    SQ:{down:false,up:false},
    SW:{down:false,up:false},
    SE:{down:false,up:false},
    SR:{down:false,up:false},
    ST:{down:false,up:false},
    SY:{down:false,up:false},
    SU:{down:false,up:false},
    SI:{down:false,up:false},
    SO:{down:false,up:false},
    SP:{down:false,up:false},
    SA:{down:false,up:false},
    SS:{down:false,up:false},
    SD:{down:false,up:false},
    SF:{down:false,up:false},
    SG:{down:false,up:false},
    SH:{down:false,up:false},
    SJ:{down:false,up:false},
    SK:{down:false,up:false},
    SL:{down:false,up:false},
    SZ:{down:false,up:false},
    SX:{down:false,up:false},
    SC:{down:false,up:false},
    SV:{down:false,up:false},
    SB:{down:false,up:false},
    SN:{down:false,up:false},
    SM:{down:false,up:false},
    //ALT + KEY
    AQ:{down:false,up:false},
    AW:{down:false,up:false},
    AE:{down:false,up:false},
    AR:{down:false,up:false},
    AT:{down:false,up:false},
    AY:{down:false,up:false},
    AU:{down:false,up:false},
    AI:{down:false,up:false},
    AO:{down:false,up:false},
    AP:{down:false,up:false},
    AA:{down:false,up:false},
    AS:{down:false,up:false},
    AD:{down:false,up:false},
    AF:{down:false,up:false},
    AG:{down:false,up:false},
    AH:{down:false,up:false},
    AJ:{down:false,up:false},
    AK:{down:false,up:false},
    AL:{down:false,up:false},
    AZ:{down:false,up:false},
    AX:{down:false,up:false},
    AC:{down:false,up:false},
    AV:{down:false,up:false},
    AB:{down:false,up:false},
    AN:{down:false,up:false},
    AM:{down:false,up:false},
    //CTRL + KEY
    CQ:{down:false,up:false},
    CW:{down:false,up:false},
    CE:{down:false,up:false},
    CR:{down:false,up:false},
    CT:{down:false,up:false},
    CY:{down:false,up:false},
    CU:{down:false,up:false},
    CI:{down:false,up:false},
    CO:{down:false,up:false},
    CP:{down:false,up:false},
    CA:{down:false,up:false},
    CS:{down:false,up:false},
    CD:{down:false,up:false},
    CF:{down:false,up:false},
    CG:{down:false,up:false},
    CH:{down:false,up:false},
    CJ:{down:false,up:false},
    CK:{down:false,up:false},
    CL:{down:false,up:false},
    CZ:{down:false,up:false},
    CX:{down:false,up:false},
    CC:{down:false,up:false},
    CV:{down:false,up:false},
    CB:{down:false,up:false},
    CN:{down:false,up:false},
    CM:{down:false,up:false},
    //ALT+SHIFT+KEY
    ASQ:{down:false,up:false},
    ASW:{down:false,up:false},
    ASE:{down:false,up:false},
    ASR:{down:false,up:false},
    AST:{down:false,up:false},
    ASY:{down:false,up:false},
    ASU:{down:false,up:false},
    ASI:{down:false,up:false},
    ASO:{down:false,up:false},
    ASP:{down:false,up:false},
    ASA:{down:false,up:false},
    ASS:{down:false,up:false},
    ASD:{down:false,up:false},
    ASF:{down:false,up:false},
    ASG:{down:false,up:false},
    ASH:{down:false,up:false},
    ASJ:{down:false,up:false},
    ASK:{down:false,up:false},
    ASL:{down:false,up:false},
    ASZ:{down:false,up:false},
    ASX:{down:false,up:false},
    ASC:{down:false,up:false},
    ASV:{down:false,up:false},
    ASB:{down:false,up:false},
    ASN:{down:false,up:false},
    ASM:{down:false,up:false},
    //CTRL+SHIFT+KEY
    CSQ:{down:false,up:false},
    CSW:{down:false,up:false},
    CSE:{down:false,up:false},
    CSR:{down:false,up:false},
    CST:{down:false,up:false},
    CSY:{down:false,up:false},
    CSU:{down:false,up:false},
    CSI:{down:false,up:false},
    CSO:{down:false,up:false},
    CSP:{down:false,up:false},
    CSA:{down:false,up:false},
    CSS:{down:false,up:false},
    CSD:{down:false,up:false},
    CSF:{down:false,up:false},
    CSG:{down:false,up:false},
    CSH:{down:false,up:false},
    CSJ:{down:false,up:false},
    CSK:{down:false,up:false},
    CSL:{down:false,up:false},
    CSZ:{down:false,up:false},
    CSX:{down:false,up:false},
    CSC:{down:false,up:false},
    CSV:{down:false,up:false},
    CSB:{down:false,up:false},
    CSN:{down:false,up:false},
    CSM:{down:false,up:false},
    //CTRL+ALT+SHIFT+KEY
    CASQ:{down:false,up:false},
    CASW:{down:false,up:false},
    CASE:{down:false,up:false},
    CASR:{down:false,up:false},
    CAST:{down:false,up:false},
    CASY:{down:false,up:false},
    CASU:{down:false,up:false},
    CASI:{down:false,up:false},
    CASO:{down:false,up:false},
    CASP:{down:false,up:false},
    CASA:{down:false,up:false},
    CASS:{down:false,up:false},
    CASD:{down:false,up:false},
    CASF:{down:false,up:false},
    CASG:{down:false,up:false},
    CASH:{down:false,up:false},
    CASJ:{down:false,up:false},
    CASK:{down:false,up:false},
    CASL:{down:false,up:false},
    CASZ:{down:false,up:false},
    CASX:{down:false,up:false},
    CASC:{down:false,up:false},
    CASV:{down:false,up:false},
    CASB:{down:false,up:false},
    CASN:{down:false,up:false},
    CASM:{down:false,up:false},
    //CTRL+ALT+KEY
    CAQ:{down:false,up:false},
    CAW:{down:false,up:false},
    CAE:{down:false,up:false},
    CAR:{down:false,up:false},
    CAT:{down:false,up:false},
    CAY:{down:false,up:false},
    CAU:{down:false,up:false},
    CAI:{down:false,up:false},
    CAO:{down:false,up:false},
    CAP:{down:false,up:false},
    CAA:{down:false,up:false},
    CAS:{down:false,up:false},
    CAD:{down:false,up:false},
    CAF:{down:false,up:false},
    CAG:{down:false,up:false},
    CAH:{down:false,up:false},
    CAJ:{down:false,up:false},
    CAK:{down:false,up:false},
    CAL:{down:false,up:false},
    CAZ:{down:false,up:false},
    CAX:{down:false,up:false},
    CAC:{down:false,up:false},
    CAV:{down:false,up:false},
    CAB:{down:false,up:false},
    CAN:{down:false,up:false},
    CAM:{down:false,up:false}
    
};
axe.keysArray = [ "Q","W","E","R","T","Y","U","I","O","P",'A','S','D','F',
		 'G','H','J','K','L','Z','X','C','V','B','N','M'];
axe.keyCaptureDown = (function(){
    var code = 0;
    var ch = "";
    return function(e){
	e = e || window.event;//S->A->C
	code = e.keyCode;

	//check the special keys
	if(code === 37){
	    axe.keys.LEFT.down = true;
	    return;
	}else if(code === 38){
	    axe.keys.UP.down = true;
	    return;
	}else if(code === 39){
	    axe.keys.RIGHT.down = true;
	    return;
	}else if(code === 40){
	    axe.keys.DOWN.down = true;
	    return;
	}else if(code === 16){
	    axe.keys.SHIFT.down = true;
	    return;
	}else if(code === 17){
	    axe.keys.CTRL.down = true;
	    return;
	}else if(code === 18){
	    axe.keys.ALT.down = true;
	    return;
	}else if(code === 32){
	    axe.keys.SPACE.down = true;
	    return;
	}else if(code === 13){
	    axe.keys.ENTER.down = true;
	    return;
	}else if(code === 9){
	    axe.keys.TAB.down = true;
	    return;
	}else if(code === 8){
	    axe.keys.BSPACE.down = true;
	    return;
	}else if(code === 27){
	    axe.keys.ESC.down = true;
	    return;
	}else if(code === 49){
	    axe.keys["1"].down = true;
	    return;
	}else if(code === 50){
	    axe.keys["2"].down = true;
	    return;
	}else if(code === 51){
	    axe.keys["3"].down = true;
	    return;
	}else if(code === 52){
	    axe.keys["4"].down = true;
	    return;
	}else if(code === 53){
	    axe.keys["5"].down = true;
	    return;
	}else if(code === 54){
	    axe.keys["6"].down = true;
	    return;
	}else if(code === 55){
	    axe.keys["7"].down = true;
	    return;
	}else if(code === 56){
	    axe.keys["8"].down = true;
	    return;
	}else if(code === 57){
	    axe.keys["9"].down = true;
	    return;
	}else if(code === 48){
	    axe.keys["0"].down = true;
	    return;
	}else if(code === 97){
	    axe.keys.N1.down = true;
	    return;
	}else if(code === 98){
	    axe.keys.N2.down = true;
	    return;
	}else if(code === 99){
	    axe.keys.N3.down = true;
	    return;
	}else if(code === 100){
	    axe.keys.N4.down = true;
	    return;
	}else if(code === 101){
	    axe.keys.N5.down = true;
	    return;
	}else if(code === 102){
	    axe.keys.N6.down = true;
	    return;
	}else if(code === 103){
	    axe.keys.N7.down = true;
	    return;
	}else if(code === 104){
	    axe.keys.N8.down = true;
	    return;
	}else if(code === 105){
	    axe.keys.N9.down = true;
	    return;
	}else if(code === 96){
	    axe.keys.N0.down = true;
	    return;
	}else if(code === 192){
	   axe.keys['`'].down = true;
	    return;
	}
	
	
	//make the keyobject name
	ch = String.fromCharCode(code);
	
	//add on the modifier keys
	if(e.shiftKey){
	    axe.keys.SHIFT.down = false;
	    ch = "S"+ch;
	}
	if(e.altKey){
	    axe.keys.ALT.down = false;
	    ch = "A"+ch;
	}
	if(e.ctrlKey){
	    axe.keys.CTRL.down = false;
	    ch= "C"+ch;
	}
	
	if(axe.keys[ch]){
	    axe.keys[ch].down = true;
	}
    };
})();


axe.keyCaptureUp = (function(){
    var code = 0;
    var ch = "";
    return function(e){
	e = e || window.event;//S->A->C
	code = e.keyCode;
	
	//check the special keys
	if(code === 37){
	    axe.keys.LEFT.up = true;
	    axe.keys.LEFT.down = false;
	    return;
	}else if(code === 38){
	    axe.keys.UP.up = true;
	    axe.keys.UP.down = false;
	    return;
	}else if(code === 39){
	    axe.keys.RIGHT.up = true;
	     axe.keys.RIGHT.down = false;
	    return;
	}else if(code === 40){
	    axe.keys.DOWN.up = true;
	    axe.keys.DOWN.down = false;
	    return;
	}else if(code === 16){
	    axe.keys.SHIFT.up = true;
	    axe.keys.SHIFT.down = false;
	    return;
	}else if(code === 17){
	    axe.keys.CTRL.up = true;
	    axe.keys.CTRL.down = false;
	    return;
	}else if(code === 18){
	    axe.keys.ALT.up = true;
	    axe.keys.ALT.down = false;
	    return;
	}else if(code === 32){
	    axe.keys.SPACE.up = true;
	    axe.keys.SPACE.down = false;
	    return;
	}else if(code === 13){
	    axe.keys.ENTER.up = true;
	    axe.keys.ENTER.down = false;
	    return;
	}else if(code === 9){
	    axe.keys.TAB.up = true;
	    axe.keys.TAB.down = false;
	    return;
	}else if(code === 8){
	    axe.keys.BSPACE.up = true;
	    axe.keys.BSPACE.down = false;
	    return;
	}else if(code === 27){
	    axe.keys.ESC.up = true;
	    axe.keys.ESC.down = false;
	    return;
	}else if(code === 49){
	    axe.keys["1"].up = true;
	    axe.keys["1"].down = false;
	    return;
	}else if(code === 50){
	    axe.keys["2"].up = true;
	    axe.keys["2"].down = false;
	    return;
	}else if(code === 51){
	    axe.keys["3"].up = true;
	    axe.keys["3"].down = false;
	    return;
	}else if(code === 52){
	    axe.keys["4"].up = true;
	    axe.keys["4"].down = false;
	    return;
	}else if(code === 53){
	    axe.keys["5"].up = true;
	    axe.keys["5"].down = false;
	    return;
	}else if(code === 54){
	    axe.keys["6"].up = true;
	    axe.keys["6"].down = false;
	    return;
	}else if(code === 55){
	    axe.keys["7"].up = true;
	    axe.keys["7"].down = false;
	    return;
	}else if(code === 56){
	    axe.keys["8"].up = true;
	    axe.keys["8"].down = false;
	    return;
	}else if(code === 57){
	    axe.keys["9"].up = true;
	    axe.keys["9"].down = false;
	    return;
	}else if(code === 48){
	    axe.keys["0"].up = true;
	    axe.keys["0"].down = false;
	    return;
	}else if(code === 97){
	    axe.keys.N1.up = true;
	    axe.keys.N1.down = false;
	    return;
	}else if(code === 98){
	    axe.keys.N2.up = true;
	    axe.keys.N2.down = false;
	    return;
	}else if(code === 99){
	    axe.keys.N3.up = true;
	    axe.keys.N3.down = false;
	    return;
	}else if(code === 100){
	    axe.keys.N4.up = true;
	    axe.keys.N4.down = false;
	    return;
	}else if(code === 101){
	    axe.keys.N5.up = true;
	    axe.keys.N5.down = false;
	    return;
	}else if(code === 102){
	    axe.keys.N6.up = true;
	    axe.keys.N6.down = false;
	    return;
	}else if(code === 103){
	    axe.keys.N7.up = true;
	    axe.keys.N7.down = false;
	    return;
	}else if(code === 104){
	    axe.keys.N8.up = true;
	    axe.keys.N8.down = false;
	    return;
	}else if(code === 105){
	    axe.keys.N9.up = true;
	    axe.keys.N9.down = false;
	    return;
	}else if(code === 96){
	    axe.keys.N0.up = true;
	    axe.keys.N0.down = false;
	    return;
	}else if(code === 192){
		axe.keys['`'].up = true;
	   axe.keys['`'].down = false;
	   return;
	}
	
	//make the keyobject name
	ch = String.fromCharCode(code);
	
	//add on the modifier keys
	if(e.shiftKey){
	    axe.keys.SHIFT.up = false;
	    ch = "S"+ch;
	}
	if(e.altKey){
	    axe.keys.ALT.up = false;
	    ch = "A"+ch;
	}
	if(e.ctrlKey){
	    axe.keys.CTRL.up = false;
	    ch= "C"+ch;
	}
	if(axe.keys[ch]){
	    axe.keys[ch].down = false;
	    axe.keys[ch].up = true;
	}
    };
})();

axe.mouse = {};
axe.mouse.offsetx = 0;
axe.mouse.offsety = 0;
axe.mouse.pos = [0,0];
axe.mouse.leftDown = false;
axe.mouse.leftUp = false;

//this is an awesome function, with this you can add
//axis events to any object you want. It's very light.
//If your event returns 10001 it is removed.
axe.addEventsTo = function(obj){
	var events = {};
	
	//adds an event to the event list
	obj.addEvent = function(type, func){
		if(!events[type]){
			events[type] = [];
		}
		
		events[type].push(func);
	};
	
	obj.getEvents = function(){return events};
	//fires events, removes the even if it returns "remove"
	obj.fireEvent = function(type, data){
		if(!events[type]){
			return;
		}
		var type_array = events[type];
		var l = type_array.length;
		for(var i = 0; i<l; i++){
			var e = type_array[i];
			
			if(e){				
				if(e(data) === 10001){
					type_array[i] = undefined; 
					_clean_events();
				}
			}
		}
	};
	
	obj.clearEvents = function(type){
		if(!events[type]){
			return;
		}
		var type_array = events[type];
		var l = type_array.length;
		for(var i = 0; i<l; i++){
			var e = type_array[i];
			
			if(e){
				type_array[i] = undefined;
				
			}
		}
		_clean_events();
	};
	obj._resetAllEvents = function(){
		for(type in events){
			if(events.hasOwnProperty(type)){
				var l = events[type].length;
				for(var i = 0; i<l; i++){
					events[type][i] = undefined;
				}
				events[type] = [];
			}
		}
	}
	//cleans up removed events
	_clean_events = function(){
		for(type in events){
			if(events.hasOwnProperty(type)){
				var new_type_array = [];
		
				events[type].map(function(e){
					if(e)new_type_array.push(e);
				});
				
				events[type] = new_type_array;
			}
		}
	};
};
axe.fireEvent = function(type){
	axe.addEventsTo(axe);
	axe.fireEvent(type);
}
axe.addEvent = function(type){
	axe.addEventsTo(axe);
	axe.addEvent(type)
}
axe.clear = function(){
    for(var k in axe.keys){
		if(axe.keys[k].up){
			axe.keys[k].up = false;
		}
    }
    axe.mouse.leftUp = false;
    axe.mouse.midUp = false;
    axe.mouse.rightUp = false;
};

axe.init = function(fps,parent){
    fps = fps || 30;
    parent = parent || window;
    //add event listeners
    if(!axe._clean_events){
    	axe.addEventsTo(axe);
    }
    //key event listeners
    window.addEventListener("keydown", axe.keyCaptureDown);
    window.addEventListener("keyup", axe.keyCaptureUp);
    //mouse event listeners
    ax.canvas.addEventListener("mousemove",function(e){
		var posx = e.offsetX || e.layerX || e.screenX; 
		var posy = e.offsetY || e.layerY || e.screenY;
		//aspect-ratio coord transfer if needed
		posx = (ax.lockRes) ? (ax.width * (posx / ax.lockDiv.width)) : posx;
		posy = (ax.lockRes) ? (ax.height * (posy / ax.lockDiv.height)) : posy;
		
		axe.mouse.pos[0] = posx;
		axe.mouse.pos[1] = posy;
		axe.fireEvent("mouseMove");
    });
   //eventually make this clear the mouse down buttons
	ax.canvas.addEventListener("mouseout",function(e){
		axe.clear();
		axe.mouse.pos[0] = ax.halfWidth;
		axe.mouse.pos[1] = ax.halfHeight;
		axe.fireEvent("mouseOut");
    });
    ax.canvas.addEventListener("mousedown",function(e){
		//event.preventDefault();
        switch(e.which || e.button){
            case 1:
               axe.mouse.leftDown = true;
					axe.fireEvent("leftDown");
                break;
            case 2:
               axe.mouse.midDown = true;
					axe.fireEvent("midDown");
                break;
            case 3:
               axe.mouse.rightDown = true;
					axe.fireEvent("rightDown");
                break;
        }

    });
    ax.canvas.addEventListener("mouseup",function(e){
        //event.preventDefault();
		switch(e.which || e.button){
            case 1:
                axe.mouse.leftDown = false;
                axe.mouse.leftUp = true;
					axe.fireEvent("leftUp");
                break;
            case 2:
                axe.mouse.midDown = false;
                axe.mouse.midUp = true;
				axe.fireEvent("midUp");
                break;
            case 3:
                axe.mouse.rightDown = false;
                axe.mouse.rightUp = true;
				axe.fireEvent("rightUp");
                break;
        }

    });
    //touch event listener
    parent.addEventListener("touchstart", function(e){
        event.preventDefault();
        axe.mouse.leftDown = true;
        var touch = e.touches[0];
        axe.mouse.pos[0] = touch.pageX;
        axe.mouse.pos[1] = touch.pageY;
		axe.fireEvent("leftDown");
    });
    parent.addEventListener("touchend", function(e){
        event.preventDefault();
        axe.mouse.leftDown = false;
        axe.mouse.leftUp = true;
		axe.fireEvent("leftUp");
    });
    parent.addEventListener("touchmove", function(e){
        event.preventDefault();
        var touch = e.touches[0];
        axe.mouse.pos[0] = touch.pageX;
        axe.mouse.pos[1] = touch.pageY;
		axe.fireEvent("mouseMove");
    });
    
    axe.keys['`'] = {down:false, up:false};
    
    var i = 0;
    var l = axe.keysArray.length;
    while(i<l){
	axe.keys[axe.keysArray[i]] = {
	    down:false,
	    up:false,
	    code:axe.keysArray[i].charCodeAt(0)
	    };
	i++;
    }
    
};
