/*Copyright(C) 2010-2013 Isaiah Gilliland
*
* This file is part of Axis.
*
*    Axis is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Lesser General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Axis is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Lesser General Public License for more details.
*
*    You should have received a copy of the GNU Lesser General Public License
*    along with Axis.  If not, see <http://www.gnu.org/licenses/>.
*/

/*Axis engine core
 * “The greatest danger for most of us is not that our aim is too high
 * and we miss it, but that it is too low and we reach it.” - Michelangelo
 */

function typeOf(value) {
	var s = typeof value;
	if(s === 'object') {
		if(value) {
			if( value instanceof Array) {
				s = 'array';
			}
		} else {
			s = 'null';
		}
	}
	return s;
}

window.requestAnimationFrame = (function(){
	//Check for each browser
	//@paul_irish function
	//Globalises this function to work on any browser as each browser has a different namespace for this
	return  window.requestAnimationFrame       ||  //Chromium 
		window.webkitRequestAnimationFrame ||  //Webkit
		window.mozRequestAnimationFrame    || //Mozilla Geko
		window.oRequestAnimationFrame      || //Opera Presto
		window.msRequestAnimationFrame     || //IE Trident?
		function(callback, element){ //Fallback function
			if(ax && ax.fps)
				var fps = ax.fps;
			else
				var fps = 60;
			window.setTimeout(callback, 1000/fps);                
		}

})();


/* Axis variables for scenegraphs + system settings
 */
var ax = {};
ax.fps = 30;
ax.debug = false;
ax._pause = false;
ax.enableConsole = false;
ax._transition = false;
ax._transition_period = 30;
ax._transition_delay = ax.fps/2;
ax.ignorFocus = false;
ax.timeSpeed = 1;
ax.movementSpeed = 1.30;
ax.width = null;
ax.height = null;
ax.halfWidth = null;
ax.halfHeight = null;
ax.scrollx = null;
ax.scrolly = null;
ax.level = null;
ax.cleanupRate = 300;
ax.clearScreen = true;
ax.ctx = null;
ax.mainScreen = null;
ax.canvas = null;
//for locking aspect ratio
ax.lockRes = false;
ax.lockDiv = {};
ax.lockDiv.width = 0;
ax.lockDiv.height = 0;
ax.resize = false;
//drawing layers
ax.playarea = [];
ax.menu = [];
//scenegraph for things that must be on top always
ax.windowActive = true;
//ax simple cmds
// Quick setting the background of the canvas
ax.setBack = function(code) {
	ax.canvas.style.backgroundColor = code;
};
ax.pauseGame = function(){
	axr._tempPauseUnpauseAll();
	axe.fireEvent("onPause");
	ax._pause = true;
}
ax.unPauseGame = function(){
	axr._tempPauseUnpauseAll(true);
	axe.fireEvent("offPause");
	ax._pause = false;
	ax._transition = true;
}
ax.pauseGameNotMusic = function(){
	axe.fireEvent("onPauseNotMusic");
	ax._pause = true;
}
ax.unPauseGameNotMusic = function(){
	axe.fireEvent("offPauseNotMusic");
	ax._pause = false;
	ax._transition = true;
}
//used when needed to iterate over every actor for some reason
ax.everyActor = function(func) {
	var layer = null;
	var act = null;
	for(var sglength = ax.playarea.length, sgi = 0; sgi < sglength; sgi++) {
		layer = ax.playarea[sgi];
		if(!layer) {
			ax.playarea[sgi] = [];
			continue;
		}
		for(var l = layer.length, i = 0; i < l; i++) {
			act = layer[i];
			if(act) {
				func(act)
			}
		}
	}

	//getting actors frommenu layer
	for(var l = ax.menu.length, i = 0; i < l; i++) {
		act = ax.menu[i];
		if(act) {//select only unkilled actors
			func(act);
		}
	}
};

ax.cleanActors = (function() {
	var cnt = 0;
	return function() {

		if(cnt >= ax.cleanupRate) {
			cnt = 0;
			//clean up and fix reference points for scenegraph layers
			for(var j = 0, layerLength = ax.playarea.length; j < layerLength; j++) {
				ax.playarea[j] = ax.playarea[j].filter(function(act) {
					return (act !== null)
				});
				for(var i = 0, l = ax.playarea[j].length; i < l; i++) {
					ax.playarea[j][i]._changeRef(i);
				}
			}
			//clean up and fix reference points for menu layer
			ax.menu = ax.menu.filter(function(act) {
				return (act !== null)
			});
			for(var i = 0, l = ax.menu.length; i < l; i++) {
				ax.menu[i]._changeRef(i);
			}
		}
		cnt++;
	};
})();

// Create game screen and/or resolution locking div
ax.makeGame = function() {
	if(ax.lockRes) {
		var lock = document.createElement('div');
		lock.setAttribute("id", "lockingDiv");
		ax.lockDiv = lock;
		lock.setAttribute("style", "position:absolute;left:50%;top:50%;");
		
		//screen 1
		var inner_screen = document.createElement('canvas');
		inner_screen.setAttribute("id", "gameScreen");
		inner_screen.setAttribute("style", "width:100%;height:100%;");
		ax.ctx = inner_screen.getContext('2d');
		
		//appending outer screen and setting canvas objs
		lock.appendChild(inner_screen);
		document.body.appendChild(lock);
		ax.canvas = inner_screen;
	} else {
		//screen 1
		var inner_screen = document.createElement('canvas');
		ax.canvas = inner_screen;
		ax.ctx = inner_screen.getContext('2d');
		inner_screen.setAttribute("id", "gameScreen");
		document.body.appendChild(inner_screen);
	}
	inner_screen.onselectstart = function () { return false; }
	inner_screen.onmousedown = function () { return false; }
	if(lock){
		lock.onselectstart = function () { return false; }
		lock.onmousedown = function () { return false; }
	}
};
// Function sets the diminsions of the game
ax.setDiminsions = function() {
	if(ax.width && ax.height && !ax.resize) {
		ax.halfWidth = ax.width / 2;
		ax.halfHeight = ax.height / 2;
		ax.scrollx = ax.width / 2;
		ax.scrolly = ax.height / 2;
		ax.canvas.width = ax.width;
		ax.canvas.height = ax.height;
	}
	if(ax.resize || ax.lockRes) {//autostretch game
		ax.resizeCanvas();
	}
	if(!ax.resize && !ax.lockDiv && (!ax.width || !ax.height)) {
		throw "Must set Axis to resize or set diminsions manually."
	}

	window.addEventListener("resize", ax.resizeCanvas);
};
// the function that resizes or stretches the canvas right
ax.resizeCanvas = function() {
	var sdim = [window.innerWidth, window.innerHeight];
	if(ax.lockRes) {
		var ratio = ax.width / ax.height;
		var newRatio = sdim[0] / sdim[1];

		if(newRatio > ratio) {
			sdim[0] = sdim[1] * ratio;
		} else {
			sdim[1] = sdim[0] / ratio;
		}

		//setting the dimensions in vars to translate mouse
		ax.lockDiv.width = sdim[0];
		ax.lockDiv.height = sdim[1];
		//setting dimensions
		ax.lockDiv.style.width = sdim[0] + "px";
		ax.lockDiv.style.height = sdim[1] + "px";
		ax.lockDiv.style.marginTop = (-sdim[1] / 2) + "px";
		ax.lockDiv.style.marginLeft = (-sdim[0] / 2) + "px";
		axe.fireEvent("lockResize");
	} else if(ax.resize) {
		//set canvas to resolution of window
		ax.width = sdim[0];
		ax.height = sdim[1];
		ax.halfWidth = ax.width / 2;
		ax.halfHeight = ax.height / 2;
		ax.canvas.width = ax.width;
		ax.canvas.height = ax.height;
		ax.scrollx = ax.halfWidth;
		ax.scrolly = ax.halfHeight;
		axe.fireEvent("stretchResize");
	}
	axe.fireEvent("resize");
}

ax.newWorld2 = function(x, y, image) {
	//for now there's just a basic background/overlay
	//what apalia will be using is just really a big image.
	//In the future a tile array will also need to be used for tiled
	//maps, not a priority now though
	var width = image.width;
	var height = image.height;

	var patterns = [];

	var pat = ax.ctx.createPattern(image, "no-repeat");

	var i = 0;
	//var l = levels.length;

	var sx, sy, px, py;

	//making return object
	var robj = {};
	robj.level = 1;
	robj.pos = [x, y];
	robj.width = width;
	robj.height = height;
	robj.draw1 = function() {
		ax.ctx.save();
		ax.ctx.translate(ax.scrollx, ax.scrolly);
		ax.ctx.fillStyle = pat;
		ax.ctx.fillRect(-ax.scrollx, -ax.scrolly, ax.width, ax.height);
		ax.ctx.restore();
	};
	robj.draw2 = function() {

	};
	//Return the level object and set it to a system variable.
	ax.level = robj;
	return robj;
};

ax.newWorld = function(x, y, levels) {
	//for now there's just a basic background/overlay
	//what apalia will be using is just really a big image.
	//In the future a tile array will also need to be used for tiled
	//maps, not a priority now though
	var width = levels[0].width;
	var height = levels[0].height;

	var i = 0;
	var l = levels.length;

	var sx, sy, px, py;

	//making return object
	var robj = {};
	robj.level = 1;
	robj.pos = [x, y];
	robj.dims = [width, height];
	robj.draw1 = function() {
		i = 0;
		/*while(i < robj.level){
		 wrl = levels[i];
		 axd.image(false, wrl, robj.pos[0], robj.pos[1]);
		 i++;
		 }*/
		sx = robj.pos[0] + width / 2 - ax.scrollx;
		sx = (sx + ax.width / 2 > width) ? width - ax.width / 2 : sx;
		sy = robj.pos[1] + height / 2 - ax.scrolly;
		sy = (sy + ax.height / 2 > height) ? height - ax.height / 2 : sy;
		px = robj.pos[0] + ax.width / 2;
		py = robj.pos[1] + ax.height / 2;
		//round them
		sx = Math.round(sx);
		sy = Math.round(sy);
		px = Math.round(px);
		py = Math.round(py);
		while(i < robj.level) {
			axd.sprite(true, levels[i++], sx, sy, ax.width, ax.height, px, py, ax.width, ax.height);
		}

	};
	robj.draw2 = function() {
		i = robj.level;
		while(i < l) {
			axd.sprite(true, levels[i++], sx, sy, ax.width, ax.height, px, py, ax.width, ax.height);
		}

	};
	//Return the level object and set it to a system variable.
	ax.level = robj;
	return robj;
};

//camera object

ax.camera = (function() {
	//hidden varibles
	var mt, moveout = null;
	//making camera object
	self = {};
	self.pos = [0, 0];
	self.vel = [0, 0];
	self.target = null;
	self.edgeLock = false;
	self.scale = 0;
	self.followDelay = 0.1;
	self.timeSpeed = 1;
	self.reset = function() {
		self.target = null;
		self.edgeLock = false;
		self.pos[0] = 0;
		self.pos[1] = 0;
		self.vel[0] = 0;
		self.vel[1] = 0;
		self.followDelay = 0.1;
	};
	self.update = (function() {
		var pos = self.pos;
		var vel = self.vel;
		var targer = null;
		var dis, disx, disy = 0;
		var timeNow = 0, time = 0, lastTimeRun = 0;

		//function that limits the camera to the edges of a level
		var limitScroll = function() {//should optimize later
			var sx = ax.halfWidth - pos[0];
			var sy = ax.halfHeight - pos[1];
			var hwidth = ax.halfWidth;
			var hheight = ax.halfHeight;

			if(Math.ceil(sx - hwidth) >= ax.level.dims[0] / 2 - hwidth) {
				sx = ax.level.dims[0] / 2;
			} else if(Math.ceil(sx - hwidth) <= (ax.level.dims[0] / 2 - hwidth) * -1) {
				sx = (ax.level.dims[0] / 2 - hwidth) * -1 + hwidth;
			}
			if(Math.ceil(sy - hheight) >= ax.level.dims[1] / 2 - hheight) {
				sy = ax.level.dims[1] / 2;
			} else if(Math.ceil(sy - hheight) <= (ax.level.dims[1] / 2 - hheight) * -1) {
				sy = (ax.level.dims[1] / 2 - hheight) * -1 + hheight;
			}
			pos[0] = (sx - hwidth) * -1;
			pos[1] = (sy - hheight) * -1;

		};

		return function(do_skip) {
			if(do_skip) {
				lastTimeRun = Date.now();
				return;
			}
			if(lastTimeRun === 0) {
				timeNow = Date.now();
				time = 1 * ax.timeSpeed * self.timeSpeed;
			} else {
				timeNow = Date.now();
				time = (((timeNow - lastTimeRun) / 100) * ax.timeSpeed * self.timeSpeed);
			}
			//update position from speed or target
			target = ax.camera.target;
			if(mt) {
				disx = mt[0] - pos[0];
				disy = mt[1] - pos[1];
				dis = Math.sqrt(Math.pow(disx, 2) + Math.pow(disy, 2));

				if(dis < 20) {
					self.vel[0] = (disx) * 0.3;
					self.vel[1] = (disy) * 0.3;
				} else {
					self.vel[0] = (disx) * 0.1;
					self.vel[1] = (disy) * 0.1;
				}
				//ending the moving
				if(dis < 1) {
					mt = null;
					self.vel[0] = 0;
					self.vel[1] = 0;
					if(self.onarrive) {
						self.onarrive();
					}
				}
				pos[0] += axu.round(vel[0] * time);
				pos[1] += axu.round(vel[1] * time);

			} else if(target) {
				var target_delay = self.followDelay;
				var target_x = target.pos[0];
				var target_y = target.pos[1];
				var lerp = axu.lerp;
				pos[0] = lerp(pos[0], target_x, target_delay);
				pos[1] = lerp(pos[1], target_y, target_delay);
			} else {
				pos[0] += axu.round(vel[0] * time);
				pos[1] += axu.round(vel[1] * time);
			}
			//update scroll values
			if(ax.camera.edgeLock) {
				if(!ax.level) {
					throw "Camera is edge locked, but there's no level set up to lock to.";
				}
				limitScroll();
			}//else {
			ax.scrollx = ax.halfWidth - pos[0];
			ax.scrolly = ax.halfHeight - pos[1];
			//}

		};
	})();
	self.draw = (function() {
		var pos = self.pos;

		return function() {
			//draw debug camera
			if(ax.debug) {
				axd.cir(false, pos[0], pos[1], 4, null, [1, 250, 0, 250]);
				axd.rect(false, pos[0], pos[1], 10, 10, null, [1, 250, 0, 250]);
			}
		};
	})();
	self.moveto = (function() {
		return function(xy, speed) {
			if(!xy) {
				self.vel[0] = 0;
				self.vel[1] = 0;
				if(self.onarrive) {
					self.onarrive();
				}
			} else {
				xy[0] = Math.floor(xy[0]);
				xy[1] = Math.floor(xy[1]);
				mt = xy;
			}
		};
	})();
	return self;
})();

/*
 * New improved actor creation, using better vector physics and
 * initialization object. initialize like so:
 * new ax.Actor({
 *      type: "enemy",
 *      pos: [100,200],
 *      vel: [2,3],
 *      width: 50,
 *      height:50,
 *      layer:1,
 *      collideRadias: 25
 * });
 */
ax.Actor = function(initObject) {//change front to layers later
	//setting up important variables
	initObject = initObject || {};
	var self = this;
	//behind the scenes stuff
	var collideRef = null;
	var sceneRef = null;
	//check if this is being used
	var moveto = null;
	var sprite = null;
	var events = [];
	self.onScreen = false;
	//check if this is being used
	//sometimes a negative value is desired
	if(initObject.alpha === undefined)
		initObject.alpha = 1.0;
	if(initObject.state === undefined)
		initObject.state = 1;

	//properties with default values
	self.id = "A" + axu.randBet(0, 99999999999999999);
	self.type = initObject.type || "blank";
	self.width = initObject.width || 50;
	self.height = initObject.height || 50;
	self.pos = initObject.pos || [0, 0];
	self.vel = initObject.vel || [0, 0];
	self.angle = initObject.angle || axu.getAngle(self.vel, true);
	self.speed = initObject.speed || axu.getLength(self.vel);
	self.acceleration = initObject.acceleration || 0;
	self.normal = (self.speed > 0) ? axu.getNormal(self.vel) : [Math.cos(self.angle / 180 * Math.PI), Math.sin(self.angle / 180 * Math.PI)];
	self.pathCollisionRadius = initObject.pathCollisionRadius;
	self.rightNormal = [self.normal[1] * -1, self.normal[0]];
	self.leftNormal = [self.normal[1], self.normal[0] * -1];
	self.collision = initObject.collision || null;
	self.customDraw = initObject.customDraw || false;
	self.parent = initObject.parent;
	self.path = initObject.path || null;
	self.aniName = initObject.aniName || null;
	self.currentFrame = initObject.currentFrame || 0;
	self.stamp = initObject.stamp || null;
	self.parent = initObject.parent;
	self.fixed = initObject.fixed || false;
	self.shrinkWrap = initObject.shrinkWrap || false;
	self.skew = initObject.skew || [0,0];
	self._domoveout = true;

	//if there is a parent, inherit certain things
	if(self.parent) {
		self.state = initObject.state || self.parent.state;
		self.rotation = initObject.rotation || self.parent.rotation;
		self.layer = (initObject.layer == null)? self.parent.layer: initObject.layer;
		self.alpha = (initObject.alpha == null)? self.parent.alpha: initObject.alpha;
		self.scale = initObject.scale || self.parent.scale;
		self.pauseFree = initObject.pauseFree || self.parent.pauseFree;
		self.timeSpeed = initObject.timeSpeed || self.parent.timeSpeed;
	} else {
		self.state = initObject.state || 1;
		self.rotation = initObject.rotation || 0.0;
		self.layer = initObject.layer || 0;
		self.alpha = (initObject.alpha == null)? 1.0: initObject.alpha;
		self.pauseFree = initObject.pauseFree || false;
		self.scale = initObject.scale || 0;
		self.timeSpeed = initObject.timeSpeed || 1;
	}
	axe.addEventsTo(self)
	//text
	self.text = {
		txt : "",
		font : "serif",
		weight : "",
		size : 12,
		color : "black",
		offset : [0, 0],
		width : self.width,
		height : self.height,
		spacing : 15
	};
	self.drawText = (function() {
		var cache, otext, ofont, osize, oweight, ocolor, owidth, oheight, ospacing;
		var text, font, size, weight, color, width, height, spacing;
		return function() {
			text = self.text.txt;
			font = self.text.font;
			size = self.text.size;
			color = self.text.color;
			width = self.text.width;
			height = self.text.height;
			weight = self.text.weight;
			spacing = self.text.spacing;

			if(otext !== text || 
				ofont !== font || 
				osize !== size || 
				ocolor !== color || 
				owidth !== width || 
				oheight !== height || 
				oweight !== weight || 
				ospacing !== spacing) {

				otext = text;
				ofont = font;
				osize = size;
				ocolor = color;
				owidth = width;
				oheight = height;
				oweight = weight;
				ospacing = spacing;

				cache = axu.makeTxtImg(self.text);
			}
			axd.image(true, cache, self.text.offset[0], self.text.offset[1]);
			return cache;
		};
	})();

	//actor methods
	//adds actor to the collide list and sets up collision properties
	self.makeCollidable = function(base) {
		if(!base) {
			throw "please supply a base number or object for collision, actor id: " + self.id;
		}
		if(typeOf(base) === "number") {
			self.collideRadius = base;
			if(typeOf(collideRef) === "null") {
				collideRef = axc.addCollider(self);
				return collideRef;
			} else {
				axc.collisions[collideRef].rad = base;
				return collideRef;
			}
		} else if(typeOf(base) === "array") {
			self.collidePoly = base;
			if(typeOf(collideRef) === "null") {
				collideRef = axc.addCollider(self);
				return collideRef;
			} else {
				axc.collisions[collideRef].poly = base;
				return collideRef;
			}
		} else if(base === "square") {
			base = axu.makeSquarePoly(self.width, self.height);
			self.collidePoly = base;
			if(typeOf(collideRef) === "null") {
				collideRef = axc.addCollider(self);
				return collideRef;
			} else {
				axc.collisions[collideRef].poly = base;
				return collideRef;
			}
		} else {
			return false;
		}
	};
	//change animation and/or frame
	self.changeAni = function(name, frame) {
		if(!name) {
			self.aniName = null;
			self.currentFrame = 0;
			return;
		}
		self.aniName = name;
		sprite = axr.anims[self.aniName];
		self.currentFrame = frame || 0;
	};

	self._update = (function() {
		var pos = self.pos, vel = self.vel;
		var c, poly, opos, ovel, angle, speed, dis, moveout, numframe = 0;
		var timeNow = 0, time = 0, lastTimeRun = 0;

		return function(do_skip) {
			if(do_skip || self.state === 0) {
				lastTimeRun = Date.now();
				return;
			}
			//set the old positions and contex if not already set
			if(!c)
				c = ax.ctx;
			if(!opos) {
				opos = [0, 0];
				opos[0] = pos[0];
				opos[1] = pos[1];
			}
			if(!ovel) {
				ovel = [0, 0];
			}
			//if there is a parent make sure to kill if the parent has
			//been killed
			if(self.parent) {
				if(self.parent.state === -1) {
					self.kill();
					return;
				}
			}

			//update acceleration
			self.speed += self.acceleration;

			poly = (poly !== self.path) ? self.path : poly;
			angle = (angle !== self.angle) ? self.angle : angle;
			speed = (speed !== self.speed) ? self.speed : speed;

			//update normals
			self.normal = (speed > 0) ? axu.getNormal(vel) : [Math.cos(angle / 180 * Math.PI), Math.sin(angle / 180 * Math.PI)];
			self.rightNormal = [self.normal[1] * -1, self.normal[0]];
			self.leftNormal = [self.normal[1], self.normal[0] * -1];

			//update the velocity based on the angle, unless it's already
			//been changed by the user, in which the angle/speed is updated
			if(!(ovel[0] === vel[0] && ovel[1] === vel[1])) {
				self.angle = angle = axu.getAngle(vel);
				self.speed = speed = axu.round((Math.sqrt(Math.pow(vel[0], 2) + Math.pow(vel[1], 2)))*100)/100;
			}
			vel[0] = axu.round((speed * Math.cos(angle / 180 * Math.PI))*100)/100;
			vel[1] = axu.round((speed * Math.sin(angle / 180 * Math.PI))*100)/100;
			ovel[0] = vel[0];
			ovel[1] = vel[1];

			//cleaning up the angles
			if(angle > 360)
				self.angle = angle = angle - 360;
			if(angle < 0)
				self.angle = angle = angle + 360;

			//check paths and update position
			if(lastTimeRun === 0) {
				timeNow = Date.now();
				time = 1 * ax.timeSpeed * self.timeSpeed;
			} else {
				timeNow = Date.now();
				time = (((timeNow - lastTimeRun) / 100) * ax.timeSpeed * self.timeSpeed);
			}
			moveout = false;
			if(poly) {
				var result = axc.pathPolyCheck(self, [vel[0] * time, vel[1] * time]);
				if(vel[0] !== result[0] && vel[1] !== result[1]) {
					moveout = true;
				}
				pos[0] += result[0];
				pos[1] += result[1];
			} else {
				//if there is no path poly then just update position
				pos[0] += vel[0] * time;
				pos[1] += vel[1] * time;
			}
			//keep fixed actors on screen
			if(self.fixed && !self.parent) {
				if(pos[0] + self.width / 2 < 0)
					pos[0] = 0 - self.width / 2;
				if(pos[0] - self.width / 2 > ax.width)
					pos[0] = ax.width + self.width / 2;
				if(pos[1] + self.height / 2 < 0)
					pos[1] = 0 - self.height / 2;
				if(pos[1] - self.height / 2 > ax.height)
					pos[1] = ax.height + self.height / 2;
			}

			lastTimeRun = timeNow;
			//update the old position
			opos[0] = pos[0];
			opos[1] = pos[1];

			//update collision position if needed
			if(collideRef !== null) {
				axc.collisions[collideRef].pos = self.realPosition();
				axc.collisions[collideRef].speed = vel;
			}
			//check mouse and set up events
			axu.isMouseOver(self, self.fixed);

			//keep alpha in bounds and apply it
			if(self.alpha > 1.0)
				self.alpha = 1.0;
			if(self.alpha < 0.0)
				self.alpha = 0.0;

			//do moveto if it exists
			if(moveto) {
				if(moveout) {
					moveto = null;
					moveout = false;
					self.speed = 0;
					self.fireEvent("onArrive", 0);
					return;
				}
				dis = dis || axu.distance(pos, moveto[0]);
				var ndis = axu.distance(pos, moveto[0]);

				if(ndis > (moveto[1] || speed)) {
					var movetoSpeed = (moveto[1]) ? moveto[1] : (dis - ndis) * 0.01 + ndis * 0.1;
					var norm = axu.getNormal([moveto[0][0] - pos[0], moveto[0][1] - pos[1]]);
					self.vel[0] = Math.round(norm[0] * movetoSpeed);
					self.vel[1] = Math.round(norm[1] * movetoSpeed);
				} else {
					//add the remaining distance to its position
					var norm = axu.getNormal([moveto[0][0] - pos[0], moveto[0][1] - pos[1]]);
					self.pos[0] += Math.round(norm[0] * ndis);
					self.pos[1] += Math.round(norm[1] * ndis);
					//remove the moveto
					dis = null;
					moveto = null;
					self.speed = 0;
					self.fireEvent("onArrive", 1);
				}

			}

		};
	})();

	self._debug = function() {
		//debug drawing
		if(ax.debug && self.onScreen) {
			var c = ax.ctx;
			var vel = self.vel;
			var pos = self.pos;
			//bounding box
			axd.rect(true, 0, 0, self.width, self.height, false, [1, 0, 0, 250]);
			if(collideRef !== null)
				axd.cir(true, 0, 0, axc.collisions[collideRef].rad, false, [1, 0, 255, 0]);
			//draw normals
			axd.drawNormals(self);
			//path poly
			if(self.path) {
				ax.ctx.restore();
				axd.poly(self.path, 0, 0);
			}
		}
	};

	self.moveto = (function() {
		var dis, angle, cnt = 0;
		pos = self.pos;
		return function(xy, speed) {
			if(!xy) {
				moveto = undefined;
				self.fireEvent("onArrive",-1);
				return;
			}
			//speed = speed || axu.distance(self.pos, xy)/ax.fps;
			//self.angle = axu.direction(pos,xy);
			xy[0] = Math.floor(xy[0]);
			xy[1] = Math.floor(xy[1]);
			moveto = [xy, speed];

		};
	})();

	self._changeRef = function(newRef) {
		sceneRef = newRef;
	};
	self._clear = function() {
		self.fireEvent("cleared");
		axc.collisions[collideRef] = null;
		sprite = undefined;
		aniName = undefined;
		stamp = undefined;
		self.state = -1;
		
	};
	self.kill = function() {
		self.fireEvent("killed");
		self.fireEvent("cleared");
		if(self.layer === "menu") {//set aside for menu items
			if(ax.menu[sceneRef])
				ax.menu[sceneRef] = null;
		} else {
			if(ax.playarea[self.layer])
				ax.playarea[self.layer][sceneRef] = null;
		}
		self.fireEvent("killed");
		self._resetAllEvents();
		axc.collisions[collideRef] = null;
		sprite = undefined;
		self.stamp = undefined;
		self.state = -1;
	};
	self._clear = function() {
		if(self.layer === "menu") {//set aside for menu items
			ax.menu[sceneRef] = null;
		} else {
			ax.playarea[self.layer][sceneRef] = null;
		}
		self._resetAllEvents();
		axc.collisions[collideRef] = null;
		sprite = undefined;
		self.stamp = undefined;
		self.state = -1;
	};
	self.chgLayer = function(lyr) {
		if(!(lyr >= 0)) {//must supply a layer to move it to
			if(lyr !== "menu") {
				throw "Must supply layer when changing layer, id:" + id;
			}
		}
		//kill off the old scenegraph reference
		if(self.layer === "menu") {//set aside for menu items
			ax.menu[sceneRef] = null;
		} else {
			ax.playarea[self.layer][sceneRef] = null;
		}
		self.layer = lyr;
		//set the new layer value
		if(self.layer === "menu") {// add menu item
			sceneRef = ax.menu.push(self) - 1;
		} else {//add the actor in the new location
			if(!ax.playarea[self.layer]) {
				ax.playarea[self.layer] = [];
			}
			sceneRef = ax.playarea[self.layer].push(self) - 1;
		}
	};
	self._draw = (function(){
		var timeNow = 0, time = 0, lastTimeRun = 0;
		return function(do_skip) {
			if(do_skip || self.state === 0) {
				lastTimeRun = Date.now();
				return;
			}
			
			var do_transform = false;
			var do_stamp = false;
			var do_sprite = false;
			var do_text = false;
			var do_pause = false;
			var do_shrink = self.shrinkWrap;
			//find reasons to not draw
			if(self.state <= -1){
				lastTimeRun = Date.now();
				return;
			}
			//check if the object is on screen
			if(!axu.isOnScreen(self)) {
				lastTimeRun = Date.now();
				return;
			}
			//check if it's paused and if it's pause immune
			if((ax._pause || ax._transition || self.state === 0) && !self.pauseFree){
				do_pause = true;
			}
			//check if there's a reason to do animation
			if(self.aniName){
				if(!sprite){
					sprite = axr.anims[self.aniName];
				}
				if(sprite)do_sprite = true;
			}
			//check if there's a reason to draw a stamp
			if(self.stamp){
				do_stamp = true;
			}
			//check if drawing text is needed
			if(self.text.txt !== ""){
				do_text = true;
			}
			//check if there's even a reason to transform
			if(do_text || do_sprite || do_stamp || self.customDraw || ax.debug){
				do_transform = true;
			}
			
			//do transformations if needed
			if(do_transform){
				var c = ax.ctx;
				c.globalAlpha = self.alpha;
		
				var pos = self.realPosition();
				pos = [axu.round(pos[0] + ax.scrollx),axu.round(pos[1] + ax.scrolly)];
				var scale = (1+self.scale + ax.camera.scale);
				var rot = scale*Math.sin(axu.deg2rad(self.rotation));
				scale *= (Math.cos(axu.deg2rad(self.rotation)));
				
				var skewx = Math.tan(axu.deg2rad(self.skew[0]));
				var skewy = Math.tan(axu.deg2rad(self.skew[1]));
				
				c.setTransform(scale, skewx+rot, skewy-rot, scale, pos[0], pos[1]);
			}
			//do sprite animation
			if(do_sprite){
				//find time since last run
				if(lastTimeRun === 0) {
					timeNow = Date.now();
					time = 1 * ax.timeSpeed * self.timeSpeed;
				} else {
					timeNow = Date.now();
					time = (((timeNow - lastTimeRun) / 100) * ax.timeSpeed * self.timeSpeed);
				}
				var frameIncrease = axu.round(time/sprite._time);
				if(frameIncrease !== 0)lastTimeRun = timeNow;
				
				var frames = self.currentFrame, anim_end = false;
				//find out if the frame we just drew is the end
				if(frames >= sprite.length){
					
					frames = frames-sprite.length;
					self.currentFrame = frames+frameIncrease;
					anim_end = true;//so event runs after draw
				} else if(frames < 0){
					frames = sprite.length + frames;
					self.currentFrame = frames+frameIncrease;
				}else { //if it's not at the end just add another frame
					if(!do_pause){
						self.currentFrame += frameIncrease;
					}
				}
				
				//do drawing
				if(do_shrink){
					sprite.draw(self, frames, 0,0, self.width, self.height);
				} else {
					sprite.draw(self, frames, 0,0);
				}
				//event
				if(anim_end){
					self.fireEvent("animationEnd", {actor:self, sprite:sprite});
				}
			}
			//do stamp
			if(!do_sprite && do_stamp){
				if(do_shrink){
					axd.image(true, self.stamp, 0, 0, self.width, self.height);
				}else {
					axd.image(true, self.stamp, 0, 0);
				}
			}
			//do text
			if(do_text){
				self.drawText();
			}
		};
	})();
	self.realPosition = function(){
		if(self.parent){
			var parentpos = self.parent.realPosition();
			var actorpos = [self.pos[0],self.pos[1]];
			return [
				parentpos[0]+actorpos[0], 
				parentpos[1]+actorpos[1]
			];
		} else {
			var actorpos = [self.pos[0],self.pos[1]];
			if(self.fixed){
				actorpos[0] -= ax.scrollx;
				actorpos[1] -= ax.scrolly;
			}
			return actorpos;
		}
	};

	//if the collide ref is set on init
	if(self.collision) {
		var base = self.collision;
		if(typeOf(base) === "number") {
			self.collideRadius = base;
			collideRef = axc.addCollider(self);
		} else if(typeOf(base) === "array") {
			self.collidePoly = base;
			collideRef = axc.addCollider(self);
		} else if(base === "square") {
			base = axu.makeSquarePoly(self.width, self.height);
			self.collidePoly = base;
			collideRef = axc.addCollider(self);
		} else {
			throw "Did not recognize " + base + " as a form of collision.";
		}
	}

	//add to layer array in scenegraph
	if(self.layer === "menu") {//set aside for menu items
		sceneRef = ax.menu.push(self) - 1;
	} else {
		if(!ax.playarea[self.layer]) {
			ax.playarea[self.layer] = [];
		}
		sceneRef = ax.playarea[self.layer].push(self) - 1;
	}
	return self;
};

//draw everything to screen
ax.draw = function() {
	//local vars to save speed
	var c = ax.ctx;
	var level = ax.level;
	var fireEvent = axe.fireEvent;
	var playarea = ax.playarea;
	var do_skip = false;
	//return if sleeping
	if(ax._transition){
		do_skip = true;
	}
	if(!ax.windowActive && !ax.ignorFocus){
		do_skip = true;
	}
	if(do_skip){
		//draw playarea actors and draw stuff
		for(var i = 0, l = playarea.length; i < l; i++) {
			if(!playarea[i]) {
				playarea[i] = [];
				continue;
			}
			playarea[i].map(function(act) {
				if(act) {
					act._draw(do_skip);
				}
			});
		}
	
		//drawing just the menu layer
		ax.menu.map(function(act) {
			if(act) {//select only unkilled actors
				act._draw(do_skip);
			}
		});
	}else {
		if(ax.clearScreen){
			axd.clr();
		}
		//draw the first layer of the level
		if(level)
			level.draw1();
		
		//draw playarea actors and draw stuff
		for(var i = 0, l = playarea.length; i < l; i++) {
			if(!playarea[i]) {
				playarea[i] = [];
				continue;
			}
			playarea[i].map(function(act) {
				if(act) {
					c.save();
					//makes sure the canvas always returns to its state
					act._draw();
					act.fireEvent("draw");
					act._debug();
					c.restore();
				}
			});
		}
		//draw the next layers in the level
		if(level)
			level.draw2();
		//execute the custom drawing code
		fireEvent("draw");
		//draw camera if needed
		ax.camera.draw();
	
		//drawing just the menu layer
		ax.menu.map(function(act) {
			if(act) {//select only unkilled actors
				c.save();
				//makes sure the canvas always returns to its state
				act._draw();
				act.fireEvent("draw");
				act._debug();
				c.restore();
			}
		});
		fireEvent("drawLast");
		ax.scriptDebugPos[1] = 10;
	}
	
};
ax.updateLoop = function() {
	//local vars
	var _transition = ax._transition;
	var fireEvent = axe.fireEvent;
	
	var olddate = Date.now();
	var do_sleep = false;
	//check for debug console call
	if((ax.debug || ax.enableConsole) && axe.keys['`'].up){
		console.log("debug Console!!!");
	}
	
	//transition period, to keep stuff from updating too fast
	//between scene transitions and moving in and out of window
	if(_transition){
		if(ax._transition_period <= 0){
			ax._transition_period = ax._transition_delay;
			ax._transition = false;
		}else {
			ax._transition_period -= 1;
		}
	}
	if(_transition){
		do_sleep = true;
	}
	if(ax._pause){
		do_sleep = true;
	}
	if(!ax.windowActive && !ax.ignorFocus){
		do_sleep = true;
	}
	
	if(!do_sleep) {
		ax.update(false);
		fireEvent("update");
		ax.camera.update();
		axc.checkCollision();
	} else {
		ax.update(do_sleep);
	}
	//axr.music.update();
	axe.clear();
	setTimeout(ax.updateLoop, 1000 / ax.fps - (Date.now() - olddate));
};

//The update command that goes through the layers and
//updates each actor
ax.update = function(do_sleep) {
	
	ax.everyActor(function(act) {
		if(do_sleep){
			if(act.pauseFree){
				act.fireEvent("update");
				act._update();
			} else {
				act._update(true);
			}
		}else {
			act._update();
			act.fireEvent("update");
		}
		
	});

	//remove killed actors
	ax.cleanActors();
};

//scene director code
ax.direct = (function() {
	var scenes = {};

	return {
		saveScene : function(name, init) {
			scenes[name] = init;
		},
		playScene : function(name, object) {
			if(!scenes[name])
				throw "Scene doesn't exist";
			ax.camera.reset();
			axe.clearEvents("update");
			axe.clearEvents("draw");
			axe.clearEvents("drawLast");
			ax.everyActor(//kill all the existing actors
			function(actor) {
				if(actor._clear)
					actor._clear();
			});
			ax.killAllScripters();
			axc.collisions = [];
			ax._transition = true;
			ax.menu = [];
			ax.playarea = [];
			ax.level = undefined;
			scenes[name](object);
		}
	};
})();

//scripted chain events and cmds
ax.scriptDebugPos = [10, 10];
ax.killAllScripters = function() {
	ax.everyActor(function(act) {
		if(act.id[0] === "S") {
			act.kill();
		}
	});
};
ax.Scripter = function() {
	var self = this;
	var timer = 0;
	var commands = [];
	var place = null;
	var pause = false;
	axe.addEventsTo(self);

	self.fire = function() {
		if(place) {
			ax.playarea[place] = null;
			place = null;
			timer = 0;
		}
		if(!ax.playarea[0]) {
			ax.playarea[0] = [];
		}
		place = ax.playarea[0].push(self) - 1;
	};

	self._update = function(do_sleep) {
		if(do_sleep || pause) {
			return;
		}

		var l = commands.length;
		var i = 0;
		var cmd;
		while(i < l) {
			cmd = commands[i++];
			if(cmd[0] === timer / ax.fps) {
				cmd[1]();
				if(i === l) {
					if(!self.loop) {
						ax.playarea[0][place] = null;
						place = null;
						timer = 0;
					} else {
						timer = 0;
					}
				}
			}
		}
		timer++;
	};
	self._changeRef = function(newRef) {
		place = newRef;
	};
	self.cmd = function(time, func) {
		commands.push([time, func]);
	};
	self.cmds = function() {
		var l = arguments.length;
		var i = 0;
		var obj;
		while(i < l) {
			obj = arguments[i++];
			commands.push([obj[0], obj[1]]);
		}
	};
	self.kill = function() {
		ax.playarea[0][place] = null;
		place = null;
		timer = 0;
	}
	self.onOff = function() {
		pause = !pause;
	};
	//fillers
	self.show = function() {
		return [timer, commands]
	};
	self.id = "S" + axu.randBet(0, 9999999999999999);
	self._debug = function() {
		if(ax.debug) {
			ax.ctx.save();
			ax.ctx.font = "10pt Arial";
			ax.ctx.fillStyle = "rgb(0,200,0)";
			ax.ctx.fillText("[ " + self.id + "  <" + Math.round(timer / ax.fps) + ":" + timer + "> ]", ax.scriptDebugPos[0], ax.scriptDebugPos[1]);
			ax.ctx.restore();
			ax.scriptDebugPos[1] += 12;
		}
	};
	self.loop = false;
	self._draw = function() {
	};
};

ax.Init = function(fps, parent) {
	//default settings
	fps = fps || 30;
	ax._transition_period = fps/2;
	ax._transition_delay = fps/2;
	
	ax.makeGame();
	//setting diminsions
	ax.setDiminsions();
	//start input capturing
	axe.init(fps, parent);
	//start collision detection
	axc.init();
	//set window events
	window.addEventListener("blur", function() {
		if(!ax.ignorFocus)axr._tempPauseUnpauseAll();
		ax.windowActive = false;
	});
	//have game pause when focus is away
	window.addEventListener("focus", function() {
		if(!ax.ignorFocus)axr._tempPauseUnpauseAll(true);
		ax.windowActive = true;
		ax._transition = true;
	});

	//start loop
	ax.fps = fps;
	setTimeout(ax.updateLoop, 1000 / ax.fps);
	setInterval("requestAnimationFrame(ax.draw)", 1000 / ax.fps);
	
};

