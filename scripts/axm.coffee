###
Copyright(C) 2010-2013 Isaiah Gilliland

 This file is part of Axis.

    Axis is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Axis is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Axis.  If not, see <http://www.gnu.org/licenses/>.
###
###
 Beginnings of Axis music manager
###

class axr.music
	timer = 0
	cur_song = null
	cur_list = null
	playlists = {}
	
	@waitTime = 120
	
	@current = -> cur_song
	@playlists = -> playlists
	
	@newPlaylist = (name, songs...)->
		for song in songs
			unless song.tagName == "AUDIO"
				throw "Playlists must be made of audio objects only."
		playlist = {
			place : 0,
			list : songs,
			name : name
		}
		playlists[name] = playlist
	
	@play = (name, place) ->
		if !name and cur_list
			name = cur_list.name
		cur_list = if playlists[name] then playlists[name] else throw "Playlist doesn't exist."
		#do something if the list given is already current
		place = place || cur_list.place
		cur_song = cur_list.list[place]
		cur_song.play()
		timer = 0
	
	@stop = ->
		if cur_song
			cur_song.stop()
			cur_song = null
			cur_list = null
	
	@next = ->
		if cur_list.place+= 1 == cur_list.list.length
			cur_list.place = 0
		cur_song.stop()
		cur_song = cur_list.list[cur_list.place]
		cur_song.play()
	
	@update = ->
		unless cur_song
			return false
			
		if !ax.windowActive and !ax.ignorFocus
			cur_song.pause()
			return false
		else if cur_song.paused
			cur_song.play()
		
		timer += 1
		
		if (@waitTime)
			if timer/ax.fps >= @waitTime
				@next()
				timer = 0
			if (!cur_song.loop) and (cur_song.currentTime >= cur_song.duration - 0.3)
				cur_song.currentTime = 0
				cur_song.play()
		else if cur_song.currentTime >= cur_song.duration - 0.3
			@next()
			timer = 0
		
	
