###
Copyright(C) 2010-2013 Isaiah Gilliland

 This file is part of Axis.

    Axis is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Axis is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Axis.  If not, see <http://www.gnu.org/licenses/>.

  <Axis Resource Manager>
###

#namespace
axr = {}
axr.images = {}
axr.anims = {}
axr.audio = {}
axr.video = {}


axr.$ = (search_id, search_class, search_tag)->
	list = []
	
	if typeOf(search_id) == "object"
		search_tag = search_id.tag
		search_class = search_id.class
		search_id = search_id.id
	
	for element in document.getElementsByTagName("*")
		id = element.getAttribute("id")
		tag = element.tagName.toLowerCase()
		_class = element.getAttribute("class")
		
		if tag == (search_tag || tag) and
		id == (search_id || id) and
		_class == (search_class || _class)
			list.push(element)
	
	if (list.length == 1 or list.length == 0) then list[0] else list


#checks if a canvas is empty and returns true if it is
axr.isCanvasEmpty = (canvas)->
	if canvas.width == 0 or canvas.height == 0
		return true
	ctx = canvas.getContext("2d")
	data = ctx.getImageData(0,0,canvas.width,canvas.height).data
	for x,pixel in data by 4
		unless data[pixel+3] == 0
			return false
	true

axr.isElementLoaded = (element, loadObj)->
	if not element then return false
	
	switch element.tagName
		when "IMG"
			if element.complete and element.width > 0 and element.height > 0
				element.loadObj = loadObj
				true
			else
				false
		when "AUDIO"
			if element.readyState > 2
				element.loadObj = loadObj
				true
			else
				false
		when "VIDEO"
			if element.readyState > 2
				element.loadObj = loadObj
				true
			else
				false
		else element.loadObj = loadObj; true

	
axr.resetAudio = (tag)->
	for own key,audio of axr.audio
		checktag = tag || audio.tag
		
		if audio.tag == checktag
			audio.stop()


axr.resetVideo = ()->
	for own key,video of axr.video
		video.stop()
		
axr._tempPauseUnpauseAll = do ->
	paused_audio_list = []
	paused_video_list = []
	
	(t_f)->
		if t_f
			#audio unpause
			for audio in paused_audio_list
				audio.play()
			#video unpause
			for video in paused_video_list
				video.play()
			
			paused_audio_list = []
			paused_video_list = []
		else
			for own key,audio of axr.audio
				if not audio.paused
					audio.pause()
					paused_video_list.push(audio)
			for own key,video of axr.video
				if not video.obj.paused
					video.pause()
					paused_video_list.push(video)
		
		true

axr.setAudioVolume = (volume, tag)->
	for own key,audio of axr.audio
		checktag = tag || audio.tag
		
		if audio.tag == checktag
			audio.volume = volume

#uid generator
axr.idGen = do ()->
	uid = {};
	
	(root = "G")->
		#find out if the root of the id exists, 
		#if it not make it, otherwise increment it
		if uid[root] == undefined
			uid[root] = 0
		else
			uid[root] += 1
		
		#return the root letter, with the root increment and date
		"#{root}#{uid[root]}#{Date.now()}"



###
* axr.failLimit ---
* A number holding the amount of times it'll let a resource 
* load fail before it tries to recreate and reload it.
* 
* axr.loadList ---
* list holding the lists of resources to load
* and manage. 
* The data should look like [[],[],[]], so that
* the developer may only load certain resources 
* at certain times.
###
axr.failLimit = 100
axr.loadList = [];

###
* Again a function for those that want it. Allows you to 
* completely remove a section of resources from memory. 
* Use in combination with axr.unload() to completely wipe
* the resources gone.
* 
* use:
*    axr.deleteSection(3);
* 
* Where the number passed is the section to delete. It does
* not default to section 0 and throws an error if you don't
* give it a section. This is because of the possibility of 
* accidental use.
###
axr.deleteSection = (sect)->
	if !sect
		throw "Must supply section to remove."
	axr.loadList[sect] = []


###
* Function that tries to release the resources of a given 
* section to the javascript garbage handler. Some things 
* like images though are cached by the browser and may have
* no effect. Even so the option to try it to gain some 
* performance is there for those that want it.
*
* use:
*    axr.unload(2);
*
* Where the number passed is the section to be unloaded.
* By default it'll rever to section 0.
###
axr.unload = (sect)->
	sect = sect || 0
	
	for item in axr.loadList[sect]
		switch item.type
			when 0 then location = axr.images
			when 1 then location = axr.audio
			when 2 then location = axr.video
			when 3 then location = axr.tiles
			when 4 then location = axt.tiles
		
		location[item.name] = undefined
		delete location[item.name]
		item.element = undefined
		item.loaded = false

axr.unloadItem = (place, name)->
	loader = place[name].loadObj
	if loader
		loader.element = undefined
		loader.loaded = false
	
	place[name] = undefined
	delete place[name]

###
* The main load function where you'll set in motion all the
* resources you've added to be loaded. Everything is loaded
* in an organized and orderly fashion. First it'll load all
* the images you've added with axr.loadImages() and wait for
* them to finish. Once they've finished it'll start loading
* all the audio you've added. And when that's done it'll 
* begin loading all the video. And then it'll cut up the 
* sprites, because all sprite images were loaded with the
* rest of the images. And then it'll create the tiles objects
* for all the tiles you've added.
* It'll return an array with the amount of resources loaded vs
* the number of resources in general and looks like [100,300].
* 
* use:
*    var loadAmount = axr.load(1);
* 
* Where the number passed is the section of resources to load. 
* default it'll load section 0.
* 
* You can use the numbers returned to get the load ration for
* loading bars like this.
*    var ratio = loadAmount[0]/loadAmount[1];
* 
* Or tell when loading is finished by checking if the two 
* numbers equal each other.
*    if(loadAmount[0] === loadAmount[1]){
*       //move forward game
*    }
* 
* NOTE! It's important to know that you should run axr.load 
* inside the game loop or an actor's update script. As each 
* time it's run moves forward the loading process, you can't 
* run it once and expect everything to be loaded. It's made
* to be run again and again until everything is loaded.
* 
###
axr.load = do ()->
	last_sect = 0
	images_done = false
	audio_done = false
	video_done = false
	sprites_done = false
	tiles_done = false
	
	(sect = 0)->
		unless sect == last_sect
			images_done = false
			audio_done = false
			video_done = false
			sprites_done = false
			tiles_done = false
			last_sect = sect
		
		list = axr.loadList[sect]
		if not list then return false
		ratio = [0,0]
		loaded = true
		
		for item in list
			ratio[1] += 1
			
			#load images first
			if item.type == 0
				if not axr.checkItem(item)
					loaded = false
				else
					ratio[0] += 1
			
			#next load audio items
			if images_done and item.type == 1
				if not axr.checkItem(item)
					loaded = false
				else
					ratio[0] += 1
			#next load video items
			if audio_done and item.type == 2
				if not axr.checkItem(item)
					loaded = false
				else
					ratio[0] += 1
			#next load sprite items
			if video_done and item.type == 3
				if not axr.checkItem(item)
					loaded = false
				else
					ratio[0] += 1
			#next load sprite items
			if sprites_done and item.type == 4
				if not axr.checkItem(item)
					loaded = false
				else
					ratio[0] += 1
		
		#part that switches loading mode
		if loaded
			if not images_done
				images_done = true
			else if not audio_done
				audio_done = true
			else if not video_done
				video_done = true
			else if not sprites_done
				sprites_done = true
			else if not tiles_done
				tiles_done = true
			else
				images_done = false
				audio_done = false
				video_done = false
				sprites_done = false
		
		ratio
	

axr.checkItem = (item)->
	makefunction = null
	switch item.type
		when 0 
			makefunction = ()-> axr.newImage(item.name, item.source)
		when 1
			makefunction = ()-> axr.newSound(item.name, item.source, item.types, item.doloop, item.tag)
		when 2
			makefunction = ()-> axr.newVideo(item.name, item.source, item.width, item.height, item.types, item.doloop)
		when 3
			makefunction = ()-> axr.newSprite(item.name, axr.images[item.name], item.xcount, item.ycount, item._time, item.length, item.onend)
		when 4
			makefunction = ()-> axr.newTile(item.name, item.source, item.num, item.nopad, item.onend)
	
	loaded = true
	if not item.element
		loaded = false
		item.element = makefunction()
	else
		if item.loaded or axr.isElementLoaded(item.element, item)
			item.loaded = true
		else
			loaded = false
			item.loadfails += 1
			if item.loadfails > axr.failLimit
				item.reloads += 1
				item.element = makefunction()
	
	loaded


#load image into load list
axr.loadImages = ()->
	sect = 0;
	for img in arguments
		if typeof(img) == "number"
		  sect = img
		  continue
		
		if not img then continue
		
		resObj = {
			type : 0,
			name : img[0],
			source : img[1],
			element : null,
			loaded : false,
			loadfails : 0,
			reloads : 0
		}
		
		if axr.loadList[sect] == undefined
			axr.loadList[sect] = []
		axr.loadList[sect].push resObj
	
	undefined

#load sounds into load list
axr.loadSounds = ()->
	sect = 0;
	for sound in arguments
		if typeof(sound) == "number"
		  sect = sound
		  continue
		
		resObj = {
			type : 1,
			name : sound[0],
			source : sound[1],
			types : sound[2],
			doloop : sound[3],
			tag : sound[4],
			element : null,
			loaded : false,
			loadfails : 0,
			reloads : 0
		}
		
		if axr.loadList[sect] == undefined
			axr.loadList[sect] = []
		axr.loadList[sect].push resObj
	
	undefined

#load videos into load list
axr.loadVideos = ()->
	sect = 0;
	for video in arguments
		if typeof(video) == "number"
		  sect = video
		  continue
		
		resObj = {
			type : 2,
			name : video[0],
			source : video[1],
			width : video[2],
			height : video[3],
			types : video[4],
			doloop : video[5],
			element : null,
			loaded : false,
			loadfails : 0,
			reloads : 0
		}
		
		if axr.loadList[sect] == undefined
			axr.loadList[sect] = []
		axr.loadList[sect].push resObj
	
	undefined

#load sprites into load list
axr.loadSprites = ()->
	sect = 0;
	
	for sprite in arguments
		if typeof(sprite) == "number"
		  sect = sprite
		  continue
		
		axr.loadImages(sect,[sprite[0],sprite[1]])
		
		resObj = {
			type : 3,
			name : sprite[0],
			source : sprite[1],
			xcount : sprite[2],
			ycount : sprite[3],
			_time : sprite[4],
			length : sprite[5],
			onend : sprite[6],
			element : null,
			loaded : false,
			loadfails : 0,
			reloads : 0
		}
		
		if axr.loadList[sect] == undefined
			axr.loadList[sect] = []
		axr.loadList[sect].push resObj
	
	undefined
	
#load sprites into load list
axr.loadTiles = ()->
	sect = 0;
	
	for tile in arguments
		if typeof(tile) == "number"
		  sect = tile
		  continue
		
		temp = tile[1].split("%d")
	
		for i in [0..tile[2]-1]
			frame = if not tile[3] then axu.padNum(i, tile[2]) else i
			axr.loadImages(sect, [tile[0]+frame, temp.join(frame)])
		
		
		resObj = {
			type : 4,
			name : tile[0],
			source : tile[1],
			num : tile[2],
			nopad : tile[3],
			onend : tile[4],
			element : null,
			loaded : false,
			loadfails : 0,
			reloads : 0
		}
		
		if axr.loadList[sect] == undefined
			axr.loadList[sect] = []
		axr.loadList[sect].push resObj
	
	undefined

axr.newImage = (name, source)->
	img = new Image()
	img.crossOrigin = "Anonymous"
	img.src = source
	axr.images[name] = img
	img
	
axr.newVideo = (name,path,width,height,types)->
	#make actual video element and a holder element that holds events and such
	video = document.createElement('video')
	holder = {obj:video}
	axe.addEventsTo(holder)
	
	#events
	video.addEventListener("ended", ()->
		holder.fireEvent("ended")
		#put looping here when needed
	)
	video.addEventListener("play", ()->
		holder.fireEvent("play")
		#put looping here when needed
	)
	video.addEventListener("pause", ()->
		holder.fireEvent("pause")
		#put looping here when needed
	)
	video.addEventListener("playing", ()->
		holder.fireEvent("playing")
		#put looping here when needed
	)
	video.addEventListener("progress", ()->
		holder.fireEvent("progress")
		#put looping here when needed
	)
	video.addEventListener("volumechange", ()->
		holder.fireEvent("volumechange")
		#put looping here when needed
	)
	video.addEventListener("stalled", ()->
		holder.fireEvent("stalled")
		#put looping here when needed
	)
	
	#setting properties and css
	video.setAttribute("style",
		"position: fixed;
		opacity: 1;
		transition: all 1s ease-in-out;
		-moz-transition: all 1s ease-in-out;
		-webkit-transition: all 1s ease-in-out;
		-o-transition: all 1s ease-in-out;
		-ms-transition: all 1s ease-in-out;")
	
	video.setAttribute("id", name)
	video.setAttribute("class", "videos")
	video.setAttribute("width", width)
	video.setAttribute("width", height)
	holder.width = video.width = width
	holder.height = video.height = height
	holder.path = path
	holder.types = types
	holder.isSet = false
	
	holder.set = (x = 0,y = 0)->
		unless holder.isSet
			document.body.appendChild(video)
			holder.isSet = true
		
		video.style.left = "#{x-width/2}px"
		video.style.top = "#{y-height/2}px"
		true
	
	holder.remove = ()->
		if holder.isSet
			video.pause()
			video.currentTime = 0;
			document.body.removeChild(axr.$(name, "videos", "video"))
			holder.isSet = false
			true
		else
			false
	
	holder.play = ()->
		video.play()
	
	holder.pause = ()->
		video.pause()
	
	holder.stop = ()->
		video.pause()
		video.currentTime = 0;
	
	holder.fade = ()->
		video.style.opacity = 0
	
	holder.appear = ()->
		video.style.opacity = 1
	
	for type in types 
		src = document.createElement("source")
		src.setAttribute("src", "#{path}.#{type}")
		
		codec = if type == "ogv" then "ogg" else type
		src.setAttribute("type", "video/#{codec}")
		video.appendChild(src)
	
	
	axr.video[name] = holder
	holder

axr.newSound = (name, path, types, doloop, tag)->
	sound = document.createElement("audio")
	axe.addEventsTo(sound)
	
	#create looping and doing events
	sound.addEventListener("ended", ()->
		sound.fireEvent("ended")
		if sound.loop
			sound.play()
	)
	
	#properties
	sound.setAttribute("id", name)
	sound.loop = doloop
	sound.path = path
	sound.types = types
	sound.tag = tag
	
	sound.stop = ()->
		sound.pause();
		sound.currentTime = 0
	
	for type in types
		src = document.createElement("source")
		src.setAttribute("src", "#{path}.#{type}")
		
		codec = if type == "mp3" then "mpeg" else type
		src.setAttribute("type", "audio/#{codec}")
		
		sound.appendChild src
		
	axr.audio[name] = sound
	sound

axr.newSprite = (name, img, xcount, ycount, _time=axu.round((1/ax.fps)*100)/10,length=(xcount*ycount), onendfunc)->
	width = Math.floor img.width/xcount
	height = Math.floor img.height/ycount
	
	sprite = {
		name : name
		img : img
		_time : _time
		width : width
		height : height
		xcount : xcount
		ycount : ycount
		length : length
	}
	#adding events to it
	axe.addEventsTo(sprite);
	if onendfunc
		sprite.addEvent("onEnd", (data)->onendfunc(data);undefined)
	
	#drawing function
	sprite.draw = (actor, frames, x, y, shwidth,shheight)->
		xc = frames%xcount
		yc = ~~ (frames/xcount)
		
		axd.sprite(true, img, xc*width, yc*height, width, height, x, y, shwidth||width, shheight||height);
		
		if frames == 0
			sprite.fireEvent("onBegin", {actor:actor, sprite:sprite})
		if frames+1 == length
			sprite.fireEvent("onEnd", {actor:actor, sprite:sprite})
		undefined
	
	axr.unloadItem(axr.images, name)
	axr.anims[name] = sprite
	sprite

axr.newTile = (name, path, num, nopad, onendfunc)->
	sprite = {
		name : name
		imgs : []
		length : num
	}
	
	#splitting path to make name
	temp = path.split("%d");
	#building list of images for animations
	for i in [0..num-1]
		frame = if not nopad then axu.padNum(i, num) else i
		nme = "#{name}#{frame}"
		img = axr.images[nme]
		if not img 
			img = axr.newImage(nme, temp.join(frame))
		
		sprite.imgs.push img
		axr.unloadItem(axr.images, nme)
	
	#adding events to it
	axe.addEventsTo(sprite);
	if onendfunc
		sprite.addEvent("onEnd", (data)->onendfunc(data);undefined)
	
	width = sprite.imgs[0].width
	height = sprite.imgs[0].height
	imgs = sprite.imgs
	sprite.draw = (actor, frames, x, y, shwidth,shheight)->
		axd.sprite(true, imgs[frames], 0, 0, width, height, x, y, shwidth||width, shheight||height);
		
		if frames == 0
			sprite.fireEvent("onBegin", {actor:actor, sprite:sprite})
		if frames+1 == num
			sprite.fireEvent("onEnd", {actor:actor, sprite:sprite})
		undefined
	
	axr.anims[name] = sprite
	sprite












window.axr = axr
