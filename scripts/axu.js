/*Copyright(C) 2010-2013 Isaiah Gilliland
*
* This file is part of Axis.
*
*    Axis is free software: you can redistribute it and/or modify
*    it under the terms of the GNU Lesser General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    Axis is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU Lesser General Public License for more details.
*
*    You should have received a copy of the GNU Lesser General Public License
*    along with Axis.  If not, see <http://www.gnu.org/licenses/>.
*/

// utility functions of axis engine

//find angle betweent two points in c: angle = atan2(y2 - y1, x2 - x1) * 180 / PI;
var axu = {};
axu.isOnScreen = function(obj){
	//check if the object is on screen
	var onScreen = true;
	
	//if an object is fixed to the screen it must always be on
	//the screen
	if(obj.fixed){ 
		obj.onScreen = true;
		return onScreen;
	}
	
	var scrollx = ax.scrollx;
	var scrolly = ax.scrolly;
	//variables to make it simpler
	var pos = obj.realPosition();
	var width = obj.width * Math.abs(obj.scale + 1);
	var height = obj.height * Math.abs(obj.scale + 1);
	
	if(pos[0]+width/2+scrollx < 0){
		onScreen = false;
	}
	if(pos[0]-width/2+scrollx > ax.width){
		onScreen = false;
	}
	if(pos[1]+height/2+scrolly < 0){
		onScreen = false;
	}
	if(pos[1]-height/2+scrolly > ax.height){
		onScreen = false;
	}
	//This first is only run if the actor was off screen and is still
	//off the screen.
	if(!obj.onScreen && !onScreen){
		return onScreen;
	
	//This second is run if the object has left the screen for the
	//first time.
	} else if(obj.onScreen && !onScreen){
		obj.onScreen = false;
		obj.fireEvent("screenExit"); //run event
		return onScreen;
	
	//This third is run if the actor has just entered the screen
	} else if(!obj.onScreen && onScreen){
		obj.onScreen = true;
		obj.fireEvent("screenEnter"); //run event
	}
	
	return onScreen;
};

axu.isMouseOver = function (obj){
	var ms;
	
	//get mouse positions based on object's fixed status. If
	//the object is fixed there isn't any need to translate mouse
	//coordinates to in game coordinates.

	ms = [axe.mouse.pos[0]-ax.scrollx,axe.mouse.pos[1]-ax.scrolly]
	if(obj.fixed){
		retMs = axe.mouse.pos;
	}else {
		retMs = ms;
	}
	
	//If the actor has been parented then the actor's parent's
	//coordinates must be added to the child actor to find their
	//real location
	var rpos = obj.realPosition();
	var posx = rpos[0];
	var posy = rpos[1];
	//multiply an object's dimensions by it's scale so the game will
	//know if the mouse is over it's actual, scaled, dimensions
	var width = obj.width * Math.abs(obj.scale + 1);
	var height = obj.height * Math.abs(obj.scale + 1);
	
	if((ms[0] > posx-width/2 && ms[0] < posx+width/2) &&
	(ms[1] > posy-height/2 && ms[1] < posy+height/2)){
		if(!obj.mhover){
			obj.fireEvent("mouseEnter", {mouse:retMs});
		}
		obj.mhover = true;
		//do mouse events
		if(axe.mouse.leftDown){
			obj.fireEvent("leftDown", {mouse:retMs});
		}
		if(axe.mouse.midDown){
			obj.fireEvent("midDown", {mouse:retMs});
		}
		if(axe.mouse.rightDown){
			obj.fireEvent("rightDown", {mouse:retMs});
		}
		if(axe.mouse.leftUp){
			obj.fireEvent("leftUp", {mouse:retMs});
		}
		if(axe.mouse.midUp){
			obj.fireEvent("midUp", {mouse:retMs});
		}
		if(axe.mouse.rightUp){
			obj.fireEvent("rightUp", {mouse:retMs});
		}
	}else{
		if(obj.mhover){
			obj.fireEvent("mouseExit", {mouse:retMs});
		}
		obj.mhover = false;
	}
};

axu.makeImage = function(data){
	if(!data.height || !data.width){
		throw "Must supply axu.makeImage height and width of image.";
	}
	if(!data.code){
		throw "Must supply axu.makeImage a function to call."
	}
	var img = document.createElement("canvas");
	img.height = data.height;
	img.width = data.width
	data.code(img, img.getContext("2d"));
	
	return img;
};

axu.makeTxtImg = function (txt){
	var img = document.createElement("canvas");
	var ctx = img.getContext("2d");
	var font = txt.weight + " " + txt.size + "pt " +txt.font;
	var textarray = axu.wordWrap(txt.txt, txt.width, font);
	
	img.width = txt.width;
	img.height = txt.height;
	/*ctx.fillStyle = "red";
	ctx.fillRect(0,0,img.width,img.height);/**/
	ctx.fillStyle = txt.color;
	ctx.font = font;
	
	for(var i = 0,l = textarray.length; i<l; i++){
		ctx.fillText(textarray[i], 0, txt.size*1.2+txt.spacing*i);  
	}
	return img;
};

axu.wordWrap = function (text, width, font){
	ax.ctx.save();
	ax.ctx.font = font;
    var buffer = [];
    var txtRaw = text.split(" ");
	var lineraw = "";
	var line = "";
	
    for(var i = 0,l = txtRaw.length; i < l; i++){
		line = lineraw + txtRaw[i];
		
		if(ax.ctx.measureText(line).width >= width){
			
			buffer.push(lineraw);
			lineraw = "";
			line = "";
		}
		lineraw = lineraw + txtRaw[i] + " ";
		if(i+1 === l){
			buffer.push(line);
		}
    }
	ax.ctx.restore();
	return buffer;
};

axu.flattenArray = function(array){
	var flat = [];
	for (var i = 0, l = array.length; i < l; i++){
		flat = flat.concat((typeOf(array[i]) !== "array") ? array[i] : axu.flattenArray(array[i]));
	}
	return flat;
};

axu.largestOfArray = function (array){
	return Math.max.apply( Math, array );
};
axu.leastOfArray = function (array){
	return Math.min.apply( Math, array );
};

axu.makeSquarePoly = function(width, height){
	return [
		[-width/2,-height/2],
		[width/2,-height/2],
		[width/2,height/2],
		[-width/2,height/2]
	];	
};

axu.offsetPoly = function(poly, offset){
	var newPoly = [];
	poly.map(function(coord){
		newPoly.push([coord[0]+offset[0],  coord[1]+offset[1]]);
	});
	return newPoly;
};

//give it two angles and it returns true if they are facing
//within the degree spacing you give it
axu.isfacing = function (act1,act2,area){//thanks to John Mcdonald
	var toface = axu.direction(act1.pos,act2.pos);
	var left = toface - area;
    var right = toface + area;

    // Wrap the left and right angles
    if(left < 0){ left += 360; }
    if(right >= 360){ right -= 360; }

    // Do 2 checks:
    // - Where the left is smaller than the right, we should be between both of them.
    // - Where the either the left or right has wrapped around 360 (left > right),
    //   we only need to be facing such that we are either greater than the
    //   left OR less than the right
    if ((left < right && act1.angle > left && act1.angle < right) ||
        (left > right && (act1.angle > left || act1.angle < right))){
        return true;
    }
    return false;
};


axu.findSideOf = function(v1,v2){
   return [axu.sign(v1[0]-v2[0]),axu.sign(v1[1]-v2[1])]; 
};

axu.findBounce = function(obj_angle, wall_angle){
	var offset = wall_angle - 90;
	var result = obj_angle - offset;
	result = 90  - result;
	result = 90 + result;
	return result + offset;
};

// Pads a number given to it with 0's.
//example: axu.padNum(33, "000") --> "033"
axu.padNum = function (num, base){
	base = base || 0;
    var i = (''+num).length;
    var l = (''+base).length;
    if(l === 1){
        return '0'+num;
    }
    
    while (i < l){
        num = '0'+num;
        i++;
    }
    return ''+num;
};

//checks if a point give is within the poly given
axu.inpoly = function(poly, x, y){
    var i = 0, j, xp = [], yp = [], c = false, npol = poly.length;
    while(i < npol){
        xp.push(poly[i][0]);
        yp.push(poly[i][1]);
        i++;
    }
    i = 0;
    j = npol-1;
    while (i < npol) {
        if ((((yp[i] <= y) && (y < yp[j])) ||
        ((yp[j] <= y) && (y < yp[i]))) &&
        (x < (xp[j] - xp[i]) * (y - yp[i]) / (yp[j] - yp[i]) + xp[i]))
        c =!c;
        
        j = i++;
    }
    return c;
};

/*
 *Useful mathematical functions
 */

//finds the angle pointing from one vector to another
axu.direction = function(v1,v2,inRadians){
    var radians = Math.atan2(v2[1]-v1[1],v2[0]-v1[0]);
    
    return (inRadians) ? radians: axu.rad2deg(radians);
};

axu.rad2deg = function(radians){
    radians *= (180/Math.PI);
    if(radians< 0 )
        return radians+360;
    else
        return radians;
};

axu.deg2rad = function(degrees){
    return degrees*(Math.PI/180);
};

axu.getLength = function (xy) {
	return Math.sqrt(xy[0]*xy[0]+xy[1]*xy[1]);
};

axu.getAngle = function (xy,inRadians){
	var angle = Math.atan2(xy[1],xy[0]);
	return (inRadians) ? angle: axu.rad2deg(angle);
};

axu.getNormal = function(xy){
	var length = axu.getLength(xy);
	return (length === 0)? [0,0] : [xy[0]/length,xy[1]/length];
};

axu.distance = function (v1, v2){
    var disx = v2[0] - v1[0];
    var disy = v2[1] - v1[1];
    
    return Math.abs(Math.sqrt(disx*disx+disy*disy));
};

axu.dotProduct = function(v1,v2){
    return v1[0]*v2[0] + v1[1]*v2[1];
};

axu.perpProduct = function(v1,v2) {
	v1 = axu.getNormal(v1);
	
	return -v1[1] * v2[0] +  v1[0] * v2[1];
};

//projects vector1 onto vector2, vector2 must be normal
axu.project = function(v1, v2){
	var dp = axu.dotProduct(v1, v2);
	v2 = axu.getNormal(v2);
	
	return [v2[0]*dp, v2[1]*dp];
};

axu.polyHeight = function(shape){
	var array = [];
	
	shape.map(function(coord){array.push(coord[1])});
	
	return axu.largestOfArray(array) - axu.leastOfArray(array);
}

axu.polyWidth = function(shape){
	var array = [];
	
	shape.map(function(coord){array.push(coord[0])});
	
	return axu.largestOfArray(array) - axu.leastOfArray(array);
}

axu.polyCentoid = function(shape){
	var i = 0,centroid = [0,0], x1,y1,x2,y2,area, signedArea = 0;
   for(var l = shape.length-1; i<l; i++){
        x1 = shape[i][0];
        y1 = shape[i][1];
		x2 = shape[i+1][0];
        y2 = shape[i+1][1];
		a = x1*y2 - x2*y1;
		signedArea += a;
		centroid[0] += (x1 + x2)*a;
		centroid[1] += (y1 + y2)*a;
    }
	
	// Do last vertex
    x1 = shape[l][0];
    y1 = shape[l][1];
    x2 = shape[0][0];
    y2 = shape[0][1];
    a = x1*y2 - x2*y1;
    signedArea += a;
    centroid[0] += (x1 + x2)*a;
    centroid[1] += (y1 + y2)*a;

    signedArea *= 0.5;
    centroid[0] /= (6*signedArea);
    centroid[1] /= (6*signedArea);

    return centroid;
};

axu.sign = function(num){
    if (num < 0) return -1;
	if (num > 0) return 1;
	return 0;
};
axu.lerp = function(val1, val2, amt) {
	return (val2 - val1)*amt+val1
};

axu.round = function(number){ 
	return (~~ (Math.abs(number)+0.5))*axu.sign(number);
};

axu.fib = (function() {
  var cache = [0,1];
  return function(n) {
    if (cache[n] === undefined) {
      cache[n] = axu.fib(n-1) + axu.fib(n-2);
    }
    return cache[n];
  };
})();

axu.randBet = function(from, to){
	return Math.floor(Math.random() * (to-from+1)+from);
};
axu.randTrue = function(base){
    base = Math.floor(base);
    var i = 0;
    var tnum = 0;
    while(i <= base){
        tnum += axu.randBet(0,1);
        i++;
    }
    if(base-tnum > base/2)
        return true;
    else
        return false;
};

axu.probability = function (prob, base){
   base = base || 100;
   base = Math.abs(Math.floor(base));
   prob = Math.abs(Math.floor(prob));
   if(prob > base)
		prob = base;
      
    var num = axu.randBet(0,base);
    if(num <= prob)
    	return true;
    else
    	return false;
};
//keeps the number given positive or stops at 0
axu.keepPositive = function(x){
    if(x < 0)
        return 0;
    else
        return x;
};

