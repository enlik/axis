###
Copyright(C) 2010-2013 Isaiah Gilliland

 This file is part of Axis.

    Axis is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Axis is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Axis.  If not, see <http://www.gnu.org/licenses/>.
###
###
 Just some test scene stuff. You can name your game files whatever you want too,
 as long as you have a function to run.
###

ax.direct.saveScene("load", ->
	axr.loadSprites(["cube","resources/spritesheet.png", 5, 6])
	axr.loadTiles(["move", "resources/tiletest/click_walk%d.png", 11]);
	
	ratio = [0,1]
	axe.addEvent "update", ->
		ratio = axr.load()
		if ratio[0] == ratio[1]
			ax.direct.playScene("main")
)

ax.direct.saveScene("main",->
	for i in [1..100]
		spmini(0,0)
)


spmini = (x,y) ->
  obj = new ax.Actor({
    pos : [x,y],
    width : 20,
    height : 20,
    layer : 1,
    aniName : "cube",
    path : [[550,0],[169,523],[-444,323],[-444,-323],[169,-523]]
  })

  
  obj.addEvent "mouseMove", (data) -> 
    obj.angle = axu.direction( obj.pos, 
    [axe.mouse.pos[0]-ax.scrollx, axe.mouse.pos[1]-ax.scrolly])
  
  obj.angle = axu.randBet 0, 360
  obj.speed = 5
  
  obj
  
window.init = () ->
  ax.width = 1366
  ax.height = 768
  ax.lockRes = true
  ax.Init 30
  ax.setBack "#888"
  #ax.debug = true
  
  ax.direct.playScene("load")







